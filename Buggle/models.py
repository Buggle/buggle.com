from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


# Create your models here.
class Post(models.Model):
    title = models.CharField(max_length=128, unique=True)
    url = models.CharField(max_length=128, unique=True, default='')
    text = models.TextField()
    date = models.DateTimeField(null=True)
    status = models.SmallIntegerField(default=1, blank=True, null=False)

    def __unicode__(self):  # For Python 2, use __str__ on Python 3
        return self.title


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    profile_pic = models.ImageField(upload_to='./Buggle/static/profile_pic/', blank=True, null=True)
    about = models.TextField(blank=True, null=True)
    nick_name = models.CharField(max_length=128, unique=True)

    def __unicode__(self):
        if self.nick_name:
            return self.nick_name
        else:
            return self.user.username
