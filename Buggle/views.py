from django.http import HttpResponseRedirect, HttpResponse
from models import Post, UserProfile
from views_methods import index_view, post_view, login_view, panel_view
from django.contrib.auth import views
from django.contrib.auth.decorators import login_required


def index(request):
    return index_view.index(request, Post)


def about(request):
    return HttpResponse("ABOUT!")


def post(request, url):
    return post_view.post(request, url, Post)


def login(request):
    return login_view.user_login(request)


def logout(request):
    views.logout(request)
    return HttpResponseRedirect('/Buggle/')


@login_required()
def panel_index(request):
    return panel_view.panel_index(request)


@login_required()
def panel_posts(request):
    return panel_view.panel_posts(request, Post)


@login_required()
def edit(request, url):
    return panel_view.edit(request, url, Post)


@login_required()
def add_post(request):
    return panel_view.add_post(request)


@login_required()
def panel_edit(request, pid):
    return panel_view.edit(request, pid, Post)


@login_required()
def panel_about(request):
    return panel_view.about(request, UserProfile)


@login_required()
def panel_delete(request, pid):
    return panel_view.delete(request, pid, Post)
