from django.conf.urls import patterns, url
import views
from django.contrib.auth import views as auth_views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^index/$', views.index, name='index'),
                       url(r'^about/$', views.about, name='about'),
                       url(r'^post/(?P<url>(\w|\d|-)*)', views.post, name='post'),
                       url(r'^login', auth_views.login, {'template_name': 'Buggle/login.html'}),
                       url(r'^logout', views.logout, name='logout'),
                       url(r'^panel/posts$', views.panel_posts, name='panel_posts'),
                       url(r'^panel/$', views.panel_index, name='panel'),
                       url(r'^panel/post/new$', views.add_post, name='add_post'),
                       url(r'^panel/posts/edit/(?P<pid>([0-9]+))', views.panel_edit, name='edit'),
                       url(r'^panel/posts/delete/(?P<pid>([0-9]+))', views.panel_delete, name='delete'),
                       url(r'^panel/about$', views.panel_about, name='panel_about'))
