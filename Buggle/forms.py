# -*- coding: utf-8 -*-

from django import forms
from django.contrib.auth.models import User
from ckeditor.widgets import CKEditorWidget

import models


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserProfileEditForm(forms.ModelForm):
    about = forms.CharField(widget=CKEditorWidget())
    nick_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    profile_pic = forms.ImageField(widget=forms.FileInput)

    class Meta:
        model = models.UserProfile
        fields = ('profile_pic', 'nick_name', 'about')


class PostEditForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget(), label="متن:")
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="عنوان:")
    url = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label="لینک:")

    class Meta:
        model = models.Post
        fields = ('title', 'url', 'text')
