from django.shortcuts import render


def post(request, url, post_object):
    post_obj = post_object.objects.get(url=url)
    time = post_obj.date
    return render(request, 'Buggle/post.html',
                  {
                      'post': post_obj,
                      'time': time,
                  })
