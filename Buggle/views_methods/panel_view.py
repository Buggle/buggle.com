import datetime

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from .. import forms


def panel_index(request):
    return render(request, 'Buggle/panel_index.html')


def panel_posts(request, post_object):
    posts = post_object.objects.filter(status=1)
    return render(request, 'Buggle/panel_posts.html',
                  {
                      'posts': posts,
                  })


def edit(request, pid, post_object):
    post = post_object.objects.get(id=pid)
    if request.method == "POST":
        form = forms.PostEditForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect('/Buggle/panel/posts')
        else:
            print form.errors
            return HttpResponse('not valid')
    else:
        form = forms.PostEditForm(instance=post)
        return render(request, 'Buggle/add_post.html', {'form': form})


def delete(request, pid, post_object):
    post = post_object.objects.get(id=pid)
    post.status = 0
    post.save()
    return HttpResponseRedirect('/Buggle/panel/posts')


def add_post(request):
    if request.method == "POST":
        form = forms.PostEditForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.date = datetime.datetime.now()
            post.save()
            return HttpResponseRedirect('/Buggle/panel/posts')
        else:
            print form.errors
            return HttpResponse('not valid')
    else:
        form = forms.PostEditForm()
        return render(request, 'Buggle/add_post.html', {'form': form})


def about(request, profile_obj):
    profile = profile_obj.objects.get(user=request.user)
    if profile.profile_pic:
        profile_pic = str(profile.profile_pic).split('Buggle')[1]
    else:
        profile_pic = None
    if request.method == "POST":
        form = forms.UserProfileEditForm(request.POST, instance=profile)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return HttpResponseRedirect('/Buggle/panel')
        else:
            print form.errors
            return HttpResponse('not valid')
    else:
        form = forms.UserProfileEditForm(instance=profile)
        print profile_pic
        return render(request, 'Buggle/panel_about.html', {'form': form, 'profile': profile_pic})
