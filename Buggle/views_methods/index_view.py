from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def index(request, post_object):
    posts_list = post_object.objects.filter(status=1).order_by('-id')
    paginator = Paginator(posts_list, 2)
    p = request.GET.get('p')
    if p is None:
        p = '1'
    page = int(p)
    if page < paginator.num_pages:
        older = True
    else:
        older = False
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    authenticated = False
    if request.user.is_authenticated():
        authenticated = True
    return render(request, 'Buggle/index.html',
                  {
                      'posts': posts,
                      'older': older,
                      'next_page': page + 1,
                      'previous_page': page - 1,
                      'user': authenticated
                  })
