# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Buggle', '0007_post_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='header',
            field=models.ImageField(null=True, upload_to=b'./Buggle/static/img/', blank=True),
        ),
    ]
