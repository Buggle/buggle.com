# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Buggle', '0009_userprofile'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='header',
        ),
    ]
